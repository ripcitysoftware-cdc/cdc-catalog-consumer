package com.ripcitysoftware.productbff;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import io.pactfoundation.consumer.dsl.LambdaDsl;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.core.IsEqual.equalTo;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE,
    properties = "product-service.base-url:http://localhost:9091",
    classes = ProductCatalogClient.class)
public class ProductCatalogClientTest {
    @Rule
    public PactProviderRuleMk2 providerRuleMk2 =
            new PactProviderRuleMk2("productServiceProvider",
                    "localhost", 9091, this);

    @Autowired
    private ProductCatalogClient productCatalogClient;

    @Test
    @PactVerification
    public void test_product_details() {
        Product product = productCatalogClient.getProduct(2L);
        Assert.assertThat(product.getId(), equalTo(2L));
        Assert.assertThat(product.getName(), equalTo("KitKat Chocolate Bar"));
        Assert.assertThat(product.getDescription(),
                equalTo("KitKat is a perfect balance of chocolate and wafer first launched in the UK"));
    }


    @Pact(state = "product details",
            provider = "productServiceProvider",
            consumer = "productCatalogClient")
    public RequestResponsePact pactProductExists(PactDslWithProvider builder) {
        return builder.given("Product 2 details")
                .uponReceiving("A request for /products/2/details")
                .path("/products/2/details")
                .method("GET")
                .willRespondWith()
                .status(200)
                .body(LambdaDsl.newJsonBody((o) -> o
                        .numberType("id", 2)
                        .stringType("name", "KitKat Chocolate Bar")
                        .stringType("description", "KitKat is a perfect balance of chocolate and wafer first launched in the UK"))
                        .build())
                .toPact();
    }



}
