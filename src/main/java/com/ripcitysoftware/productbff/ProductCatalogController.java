package com.ripcitysoftware.productbff;


import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class ProductCatalogController {
    private final ProductCatalogClient productCatalogClient;

    public ProductCatalogController(
            ProductCatalogClient productCatalogClient) {
        this.productCatalogClient = productCatalogClient;
    }

    @GetMapping("/products/{id}")
    @ResponseBody
    public Product getProduct(@PathVariable("id") Long id) {
        log.info("/products/{} was invoked!", id);
        return productCatalogClient.getProduct(1L);
    }
}
