package com.ripcitysoftware.productbff;

import lombok.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Service
public class ProductCatalogClient {
    private final RestTemplate restTemplate;

    public ProductCatalogClient(@Value("${product-service.base-url}") String baseUrl) {
        restTemplate = new RestTemplateBuilder().rootUri(baseUrl).build();
    }

    public Product getProduct(Long id) {
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .path("/products/{id}/details")
                .buildAndExpand(Long.toString(id));
        return restTemplate.getForObject(uriComponents.toString(), Product.class);
    }
}
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
class Product {
    private Long id;
    private String name;
    private String description;
}
